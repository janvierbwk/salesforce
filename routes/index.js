import express from 'express'
import jsforce from'jsforce'
import getAccount from '../controller/Account';

const routes = express.Router();
 routes.get('/accounts', getAccount);
export default routes